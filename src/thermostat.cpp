#include <cstdlib>

#include "thermostat.h"

namespace emsesp {

uuid::log::Logger ThermostatDevice::logger_{F_(thermostat), uuid::log::Facility::DAEMON};

void ThermostatDevice::loop()
{
    if(mThermostat.pUpdate.processUpdateCtr++ % mThermostat.pUpdate.tempUpdateValue == 0)
    {
        mThermostat.tempSensor.process();
        mThermostat.SVcurrent.temperature = mThermostat.tempSensor.getTemperature();
        mThermostat.SVcurrent.humidity = mThermostat.tempSensor.getHumidity();
        mThermostat.SVcurrent.relativeTemp = mThermostat.tempSensor.getRelTemp();
        mThermostat.display.process(mThermostat.SVcurrent.temperature , 
            mThermostat.setPointTemp, mThermostat.SVcurrent.humidity, 
            mThermostat.SVcurrent.relativeTemp);
        if(compareSensorValues(&mThermostat.SVcurrent, &mThermostat.SVprevious, 0.1F, 1.0F))
        {
            publishValues(mThermostat.SVcurrent, mThermostat.setPointTemp);
            //Copy the current values to the previous.
            memcpy_P(&mThermostat.SVprevious, &mThermostat.SVcurrent, sizeof(TempSensorValue));
        }
        
    }
}

void ThermostatDevice::start()
{
    mThermostat.pUpdate.tempUpdateValue = 500; //every Nth iteration
    mThermostat.pUpdate.processUpdateCtr = 0;

    mThermostat.setPointTemp = 20.0;
    memset(&(mThermostat.SVprevious), 0, sizeof(TempSensorValue));

    mThermostat.tempSensor.initialize();
    mThermostat.display.initialize();

    Mqtt::subscribe("setpoint", [=](const char * setpoint) { return processSetpoint(setpoint); });
}


bool ThermostatDevice::compareSensorValues(TempSensorValue *current, TempSensorValue *previous, float diff, float humDiff)
{
    if((current->temperature < (previous->temperature - diff)) || 
       (current->temperature > (previous->temperature + diff)))
    {
        return true;
    }
    if((current->humidity < (previous->humidity - humDiff)) || 
       (current->humidity > (previous->humidity + humDiff)))
    {
        return true;
    }
    //No need to check the relative temp, since if both changed, this also changed.

    return false; //Match
}

// send all thermostat data as a JSON package to MQTT
void ThermostatDevice::publishValues(TempSensorValue value, float setpoint)
{
    DynamicJsonDocument doc(100);
    uint8_t             mqtt_format_ = Mqtt::mqtt_format();

    if(mqtt_format_ != Mqtt::Format::SINGLE){
        LOG_INFO(F("[Thermostat] only MQTT type SINGLE supported "));
    }
    //for (const auto & sensor : sensors_) {
        //char sensorID[10]; // sensor{1-n}
    //First Temperature
        //snprintf_P(sensorID, 10, PSTR("temperature"), sensor_no);
    if (mqtt_format_ == Mqtt::Format::SINGLE) {
        // e.g. dallassensor_data = {"28-EA41-9497-0E03":23.3,"28-233D-9497-0C03":24.0}
        doc["temperature"] = (float)(value.temperature);
        doc["humidity"] = (float)(value.humidity);
        doc["relativeTemp"] = (float)(value.relativeTemp);
        doc["setpoint"] = (float)(setpoint);
    }

    // doc.shrinkToFit();
    Mqtt::publish(F("sensor"), doc.as<JsonObject>());
}

bool ThermostatDevice::processSetpoint(const char *setPoint)
{
    LOG_INFO(F("[Thermostat] Received Setpoint: %s"), setPoint);
    //float num_float = std::stof(setPoint);
    float temp = std::atof(setPoint);
    if(temp > 10.0F && temp < 30.0F)
    {
        this->mThermostat.setPointTemp = temp;
        LOG_INFO(F("[Thermostat] Updated Setpoint: %s"), setPoint);
    }
    return true;
}

} // namespace emsesp